import math


def cal_sin():
    angle_degrees = float(input("angle_degrees= "))
    return "RESULT:\nSin({}) = {}".format(angle_degrees, math.sin(math.radians(angle_degrees)))


def cal_division():
        a = float(input("input value a: "))
        b = float(input("input value b: "))
        print('{} / {} = {}'.format(a, b, (a / b)) if b != 0 else 'You cant do that')
