from math import sqrt


def cal_mult():
    """Multiplication function"""
    a = float(input("a= "))
    b = float(input("b= "))
    c = a * b
    #return 'RESULT:\n{} * {} = {}'.format(a, b, (a * b))
    return f'RESULT: \n{c}'


def cal_sq2():
    """Square root function"""
    a = float(input("a= "))
    if a != 0:
        return 'RESULT:\nSquare root of {} is {} and {}'.format(a, (sqrt(a)), -sqrt(a))
    return 'RESULT:\nSquare root of {} is {}'.format(a, (sqrt(a)))